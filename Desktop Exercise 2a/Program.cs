﻿using System;

namespace Desktop_Exercise_2a
{
  class Program
  {
    static void Main(string[] args)
    {
      ArrayTester();

      Console.Read();
    }

    private static decimal[] ArrayGenerator()
    {
      Console.WriteLine("Enter array size:");

      var size = Convert.ToInt32(Console.ReadLine());

      Console.WriteLine("\n\nEnter the lowest cost for an item:");

      var min = Convert.ToDecimal(Console.ReadLine());

      Console.WriteLine("\n\nEnter the maximum cost for an item:");

      var max = Convert.ToDecimal(Console.ReadLine());

      return ArrayFactory.GetArray(size, min, max);
    }

    private static void ArrayTester()
    {
      var arr = ArrayGenerator();

      Console.WriteLine("\n\n**** OutputArray ****\n");

      ArrayFactory.OutputArray(arr);

      Console.WriteLine("\r\n\n**** AverageArrayValue ****\n");

      Console.WriteLine(string.Format("{0:C}", ArrayFactory.AverageArrayValue(arr)));

      Console.WriteLine("\r\n\n**** MinArrayValue ****\n");

      Console.WriteLine(string.Format("{0:C}", ArrayFactory.MinArrayValue(arr)));

      Console.WriteLine("\r\n\n**** MaxArrayValue ****\n");

      Console.WriteLine(string.Format("{0:C}", ArrayFactory.MaxArrayValue(arr)));

      Console.WriteLine("\r\n\n**** Sort Array-Ascending ****\n");

      ArrayFactory.SortArrayAsc(arr);

      ArrayFactory.OutputArray(arr);

      Console.WriteLine("\r\n\n**** Sort Array - Descending ****\n");

      var arrDesc = ArrayFactory.SortArrayDesc(arr);

      ArrayFactory.OutputArray(arrDesc);
    }
  }
}
